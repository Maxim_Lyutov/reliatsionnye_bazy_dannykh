<?php
    try
    {
        $pdo = new PDO("mysql:host=localhost;dbname=global","root", "" );
        
        $sqlText = "SELECT * FROM books WHERE name LIKE :name AND author LIKE :author AND isbn LIKE :isbn ";
        $stmt = $pdo->prepare($sqlText);//, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)); // ?!
        
        $authorParam = '';
        $nameParam = '';
        $isbnParam = '';
        
       if($_GET)
       {
          if ( isset($_GET['author']) )
          {
            $authorParam = $_GET['author'] ;  
          }
          if ( isset($_GET['name']) )
          {
            $nameParam = $_GET['name'] ;  
          }
          if ( isset($_GET['isbn']) )
          {
            $isbnParam = $_GET['isbn'] ;  
          }
       }
       $stmt->execute(array(':name' =>  "%{$nameParam}%", 'author' => "%{$authorParam}%", 'isbn' => "%{$isbnParam}%" ));
       $res = $stmt->fetchAll();
    }
    catch (Exception $e) 
    {
       echo "Ошибка : " . $e->getMessage();
       exit;
    }
   //print_r($res);
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
     <title>Загрузка теста</title>
     <style>
      table {
        border-spacing: 0;
        border-collapse: collapse;
      }
      table td, table th {
        border: 1px solid #ccc;
        padding: 5px;
      }   
      table th {
        background: #eee;
      }
    </style>
  <body>
    <h1>Библиотека успешного человека</h1>
    <form method="GET">
      <input type="text" name="isbn" placeholder="ISBN" value="<?= $isbnParam;?>" />
      <input type="text" name="name" placeholder="Название книги" value="<?= $nameParam;?>" />
      <input type="text" name="author" placeholder="Автор книги" value="<?= $authorParam;?>" />
      <input type="submit" value="Поиск" />
    </form>
    <br>
    <table>
        <tr>
        <th>Название</th>
        <th>Автор</th>
        <th>Год выпуска</th>
        <th>Жанр</th>
        <th>ISBN</th>
      </tr>
<?php foreach ($res as $row ):?>
<tr>
  <td><?= $row[1];?> </td> 
  <td><?= $row[2];?> </td> 
  <td><?= $row[3];?> </td>
  <td><?= $row[5];?> </td>
  <td><?= $row[4];?> </td>
</tr>
<?php endforeach;?>
     </table>    
    </body>
</html>
